package Sensors;

/**
 * @author marco
 */
public class SensorStatus {

    private final String name;
    private final float fuellstand;

    public SensorStatus(String nam, float fuell) {
        name = nam;
        fuellstand = fuell;
    }

    public String getName() {
        return name;
    }

    public float getFuellstand() {
        return fuellstand;
    }

}
