package Sensors;

import java.io.IOException;
import java.net.*;
import java.util.Random;

/**
 * @author marco
 */

public class Sensor {

    private static String HOSTIP;
    private static final int SENDPORT = 9999;
    private static int RECEIVEPORT;

    private float fuellstand;
    private final String name;
    private DatagramSocket clientSocket;
    private DatagramSocket serverSocket;
    private byte[] receiveBuffer;

    private Sensor(String n, float f, int receive) {
        fuellstand = f;
        name = n;
        RECEIVEPORT = receive;
        receiveBuffer = new byte[1024];

        try {
            clientSocket = new DatagramSocket();
            serverSocket = new DatagramSocket(RECEIVEPORT);
        } catch (SocketException e) {
            System.out.println(e);
        }

    }

    public void startThreads() {

        Runnable send = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        send();
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        };


        Runnable receive = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    receive();
                }
            }
        };

        new Thread(send).start();
        new Thread(receive).start();
    }

    private void send() {
        try {
            byte[] sendBuffer = getFuellstand().getBytes();

            InetAddress hostIP = InetAddress.getByName(HOSTIP);
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, hostIP, SENDPORT);
            clientSocket.send(sendPacket);

        } catch (UnknownHostException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    private void receive() {
        try {
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);

            serverSocket.receive(receivePacket);
            String receiveString = new String(receivePacket.getData());
            fuellstand += 50;
            System.out.println("Sensor Refill: " + name + "\nNew Value: " + fuellstand);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private String getFuellstand() {
        Random numberGen = new Random();
        if (fuellstand > 0)
            fuellstand -= numberGen.nextDouble() * 10;

        return name + ";" + fuellstand;
    }

    public static void main(String[] args) {

        if (args[0] == null) {
            System.out.println("Please enter host IP!");
            System.exit(0);
        }

        HOSTIP = args[0];

        System.out.println("Starting Sensors...");

        Sensor s1 = new Sensor("Milk", 100.0f, 9000);
        Sensor s2 = new Sensor("Eggs", 100.0f, 9001);
        Sensor s3 = new Sensor("Butter", 100.0f, 9002);
        Sensor s4 = new Sensor("Bacon", 100.0f, 9003);

        s1.startThreads();
        s2.startThreads();
        s3.startThreads();
        s4.startThreads();
    }

}
