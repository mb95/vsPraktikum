package Store;

/**
 * Created by marco on 23/05/17.
 */

import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.eclipse.paho.client.mqttv3.*;
import rpc.Refiller;
import rpc.refillData;

import java.util.*;

public class Store extends Thread implements Refiller.Iface, MqttCallback {
    private int SERVERPORT;
    private HashMap<String, Float> priceMap;
    private HashMap<String, Float> storageAmoutMap;
    private ArrayList<refillData> orders;
    private Refiller.Processor processor;
    private MqttClient mqttClient;
    String broker = "tcp:localhost:1883";
    String storeName;

    private Store(int port, String storeId) {
        storeName = storeId;
        this.SERVERPORT = port;
        this.priceMap = new HashMap<>();
        this.orders = new ArrayList<>();
        this.storageAmoutMap = new HashMap<>();
        Random rand = new Random();
        processor = new Refiller.Processor(this);

        //initializes amount of products
        storageAmoutMap.put("Milk", 200f);
        storageAmoutMap.put("Eggs", 200f);
        storageAmoutMap.put("Butter", 200f);
        storageAmoutMap.put("Bacon", 200f);

        initMqtt(storeName);
        startStorageAmountPolling();


        //initializes products price
        priceMap.put("Milk", rand.nextFloat() * 10 + 5);
        priceMap.put("Eggs", rand.nextFloat() * 10 + 5);
        priceMap.put("Butter", rand.nextFloat() * 10 + 5);
        priceMap.put("Bacon", rand.nextFloat() * 10 + 5);
    }

    @Override
    public double checkPrice(String article) throws TException {
        if (!priceMap.containsKey(article))
            throw new TException("Article not in store!");

        return priceMap.get(article);
    }

    @Override
    public void order(String article, int units) throws TException {

        if (!priceMap.containsKey(article))
            throw new TException("Article not in store!");

        float newStorageAmount = storageAmoutMap.get(article) - units;
        storageAmoutMap.put(article, newStorageAmount);

        float totalPrice = units * priceMap.get(article);
        orders.add(new refillData(article, totalPrice));
        System.out.println("Store name: " + storeName + " price: " + totalPrice + " Amount left: " + storageAmoutMap.get(article));
    }

    private void initMqtt(String store) {

        try {
            mqttClient = new MqttClient(broker, store);
            mqttClient.connect();
            mqttClient.setCallback(this);
            mqttClient.subscribe(new String[]{"Milk", "Eggs", "Butter", "Bacon"});
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void startStorageAmountPolling() {
        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (Float f : storageAmoutMap.values()) {
                    if (f < 50) {
                        for (String s : storageAmoutMap.keySet()) {
                            if (f == storageAmoutMap.get(s)) {
                                System.out.println("ordering more " + s);
                                buyMoreProduct(s);
                            }
                        }
                    }
                }
            }
        }, 0, 3000);
    }

    private void buyMoreProduct(String productName) {
        float amountToOrder = 10;

        MqttMessage message = new MqttMessage();
        message.setPayload((storeName + ":" + amountToOrder).getBytes());
        float newArticleAmount = storageAmoutMap.get(productName) + amountToOrder;

        try {
            MqttTopic mqttTopic = mqttClient.getTopic(productName);
            mqttTopic.publish(message.getPayload(), 0, false);
            storageAmoutMap.put(productName, newArticleAmount);
            System.out.println(storeName + ":new " + productName + " amount: " + newArticleAmount);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            TServerTransport serverTransport = new TServerSocket(SERVERPORT);
            TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor));
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Store(9090, "Store0").start();
        new Store(9091, "Store1").start();
        new Store(9092, "Store2").start();
        new Store(9093, "Store3").start();


        System.out.println("Starting the thrift stores...");
    }

    @Override
    public void connectionLost(Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        String token = mqttMessage.toString().split(":")[0];
        float amountToOrder = 30;

        if (!token.contains(storeName) && !token.contains("price")) {
            return;
        }

        if(token.contains("price")) {
            float price = Float.parseFloat(mqttMessage.toString().split(":")[1]);

            if (priceMap.get(s) > price) {
                MqttMessage message = new MqttMessage();
                message.setPayload((storeName + ":" + amountToOrder).getBytes());

                try {
                    mqttClient.publish(s, message);
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }

        if(token.contains(storeName)){
            String status = mqttMessage.toString().split(":")[1];

            if(status == "ordered"){
                float newArticleAmount = storageAmoutMap.get(s) + amountToOrder;

                storageAmoutMap.put(s, newArticleAmount);
                System.out.println(storeName + ":new " + s + " amount: " + newArticleAmount);
            }

        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
