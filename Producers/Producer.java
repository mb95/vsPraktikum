package Producers;

import org.eclipse.paho.client.mqttv3.*;

import java.util.*;

/**
 * Created by marco on 15/06/17.
 */

public class Producer implements MqttCallback {
    MqttClient client;
    String producerId;
    String broker = "tcp:localhost:1883";
    String topic;
    float currentPrice;
    HashMap<String, ArrayList<Float>> orders;

    public Producer(String producer, String topic) {

        try {
            producerId = producer;
            System.out.println("Starting " + producer);
            this.topic = topic;

            orders = new HashMap<>();
            orders.put("Store0", new ArrayList<Float>());
            orders.put("Store1", new ArrayList<Float>());
            orders.put("Store2", new ArrayList<Float>());
            orders.put("Store3", new ArrayList<Float>());

            currentPrice = new Random().nextFloat() * 10 + 2;
            client = new MqttClient(broker, producer);
            client.connect();
            client.setCallback(this);
            client.subscribe(topic);

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    updatePrice();
                }
            }, 0, 10000);

        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    //method called by timer
    private void updatePrice() {
        currentPrice = new Random().nextFloat() * 10;
        System.out.println(this.producerId + " new price:" + currentPrice);

        MqttMessage message = new MqttMessage();
        message.setPayload(("price:" + currentPrice).getBytes());
        try {
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        throwable.printStackTrace();
    }

    private void handleOrder(String mess) {
        String store = mess.split(":")[0];

        if(mess.contains("ordered")){
            return;
        }
        float amountToOrder = Float.parseFloat(mess.split(":")[1]);

        MqttMessage message = new MqttMessage();
        message.setPayload((store + ":" + "ordered").getBytes());

        try {
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }

        orders.get(store).add(amountToOrder);
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        //was not sure if this was called when the producer sends messages
        if(!mqttMessage.toString().contains("price")){
            System.out.println("order taken: " + mqttMessage.toString());
            handleOrder(mqttMessage.toString());
            updatePrice();
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public static void main(String args[]) {
        new Producer("Milker", "Milk");
        new Producer("Farmer", "Eggs");
        new Producer("Cheese Maker", "Butter");
        new Producer("Butcher", "Bacon");
    }
}
