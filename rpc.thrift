namespace java rpc


struct refillData{
    1: string article;
    2: double price;
}

exception Error{
    1: string errString;
}

service Refiller{
    double checkPrice(1: string article);
    void order(1: string article, 2: i32 units);
}