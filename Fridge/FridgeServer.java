package Fridge;

import java.net.SocketException;


/**
 * @author marco
 */

public class FridgeServer {

    public static void main(String args[]) throws SocketException {
        UDPServer udpServer = new UDPServer(9999);
        HTTPServer tcpServer = new HTTPServer(udpServer.getStatusLog());

        udpServer.start();
        tcpServer.start();
    }

}
