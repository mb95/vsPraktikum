package Fridge;

import Sensors.SensorStatus;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import rpc.Refiller;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author marco
 */
public class HTTPServer extends Thread {

    private ServerSocket serverSocket;
    private ArrayList<SensorStatus> statusArray;
    private String STOREIP = "localhost";
    String SENSORIP = "localhost";

    HTTPServer(ArrayList<SensorStatus> stat) {
        try {
            serverSocket = new ServerSocket(8000);
        } catch (IOException e) {
            System.out.println(e);
        }
        statusArray = stat;
    }

    private void simulateOrder(String article) {

        int SENDPORT = 0;

        switch (article) {
            case "Milk":
                SENDPORT = 9000;
                break;
            case "Eggs":
                SENDPORT = 9001;
                break;
            case "Butter":
                SENDPORT = 9002;
                break;
            case "Bacon":
                SENDPORT = 9003;
                break;
        }

        try {
            DatagramSocket clientSocket = new DatagramSocket();
            byte[] sendBuffer = (article + " Refill").getBytes();

            InetAddress hostIP = InetAddress.getByName(SENSORIP);
            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, hostIP, SENDPORT);
            clientSocket.send(sendPacket);

        } catch (UnknownHostException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }


    }

    private double[] checkStorePrices(String article) {
        int storePorts = 9090;

        //checks stores by incrementing port number, all stores are in same IP
        double[] prices = new double[4];
        for (int i = 0; i < 4; i++) {
            int currentStore = storePorts + i;
            TTransport transport = new TSocket(STOREIP, currentStore);
            try {
                transport.open();
                TProtocol protocol = new TBinaryProtocol(transport);
                Refiller.Client client = new Refiller.Client(protocol);

                prices[i] = client.checkPrice(article);
                transport.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return prices;
    }

    private void makeOrder(String article) {
        double[] prices = checkStorePrices(article);
        double[] sortedPrices = Arrays.copyOf(prices, 4);
        Arrays.sort(sortedPrices);

        int storePorts = 9090;
        for (int i = 0; i < 4; i++) {
            if (sortedPrices[0] == prices[i]) {
                TTransport transport = new TSocket(STOREIP, storePorts + i);

                try {
                    transport.open();
                    TProtocol protocol = new TBinaryProtocol(transport);
                    Refiller.Client client = new Refiller.Client(protocol);

                    client.order(article, 50);
                    transport.close();

                    simulateOrder(article);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private String parseHeader(BufferedReader buf) {
        String header = "test";
        StringBuilder request = new StringBuilder();

        try {
            header = buf.readLine();
            while (header.length() > 0) {
                request.append(header);
                request.append("\n");
                header = buf.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return request.toString();
        }

    }


    private void handleRefill(String header) {
        if (header.contains("GET /currentStatus.html?")) {
            String[] headerComponents = header.split("\n");
            String[] articleComponents = headerComponents[0].split(" ");
            String[] url = articleComponents[1].split("\\?");
            String article = url[1];

            makeOrder(article);
        }
        else return;
    }

    private void handleRequests() {
        while (true) {
            try {
                Socket connectionSocket = serverSocket.accept();

                PrintWriter outToClient = new PrintWriter(connectionSocket.getOutputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

                String header = parseHeader(reader);

                handleRefill(header);

                if (header.contains("GET /currentStatus.html")) {
                    outToClient.println("HTTP/1.1 200 OK");
                    outToClient.println("Content-type: text/html\r\n\r\n");

                    outToClient.println(this.getResponseCurrentStatus());
                    outToClient.flush();
                    outToClient.close();
                    connectionSocket.close();
                }
                if (header.contains("GET /statusLog.html")) {
                    outToClient.println("HTTP/1.1 200 OK");
                    outToClient.println("Content-type: text/html\r\n\r\n");

                    outToClient.println(this.getResponseStatusLog());
                    outToClient.flush();
                    outToClient.close();
                    connectionSocket.close();
                } else if (header.contains("GET / HTTP/1.1")) {
                    outToClient.println("HTTP/1.1 200 OK");
                    outToClient.println("Content-type: text/html\r\n\r\n");

                    outToClient.println(this.getResponseIndex());
                    outToClient.flush();
                    outToClient.close();
                    connectionSocket.close();
                }

            } catch (IOException e) {
                System.out.println(e);
            }
        }

    }

    @Override
    public void run() {
        System.out.println("TCP Server started!");
        handleRequests();
    }

    private String getResponseIndex() {
        StringBuilder builder = new StringBuilder();
        String line;
        try {
            InputStreamReader inReader = new InputStreamReader(new FileInputStream("Fridge/html/index.html"), Charset.forName("UTF-8"));
            BufferedReader bufReader = new BufferedReader(inReader);

            while ((line = bufReader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return builder.toString();
    }

    private String getResponseCurrentStatus() {
        StringBuilder builder = new StringBuilder();
        String line;
        try {
            InputStreamReader inReader = new InputStreamReader(new FileInputStream("Fridge/html/currentStatus.html"), Charset.forName("UTF-8"));
            BufferedReader bufReader = new BufferedReader(inReader);

            while ((line = bufReader.readLine()) != null) {
                if (line.contains("<!-- Add here -->")) {
                    for (int i = statusArray.size() - 1; i > statusArray.size() - 5; i--) {
                        String sensorName = statusArray.get(i).getName();

                        builder.append("<li>" + "Sensor: " + sensorName +
                                " - Value: " + statusArray.get(i).getFuellstand()
                                + getChargeLink(sensorName) + "</li>");
                    }

                } else
                    builder.append(line);
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return builder.toString();
    }

    private String getChargeLink(String s) {
        String url = "currentStatus.html?" + s;
        String one = "<a href=";
        String two = "<button>Refill!</button> </a>";

        return one + "'" + url + "'>" + two;
    }

    private String getResponseStatusLog() {
        StringBuilder builder = new StringBuilder();
        String line;
        try {
            InputStreamReader inReader = new InputStreamReader(new FileInputStream("Fridge/html/statusLog.html"), Charset.forName("UTF-8"));
            BufferedReader bufReader = new BufferedReader(inReader);

            while ((line = bufReader.readLine()) != null) {
                if (line.contains("<!-- Add here -->")) {
                    for (int i = 0; i < statusArray.size() - 1; i++) {
                        builder.append("<li>" + "Sensor: " + statusArray.get(i).getName() +
                                " - Value: " + statusArray.get(i).getFuellstand() + "</li>");
                    }

                } else
                    builder.append(line);
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return builder.toString();
    }
}
