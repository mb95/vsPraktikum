package Fridge;

import Sensors.SensorStatus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * @author marco
 */

public class UDPServer extends Thread {

    private ArrayList<SensorStatus> statusLog;
    private DatagramSocket serverSocket;
    private byte[] receiveBuffer;

    UDPServer(int port) throws SocketException {
        serverSocket = new DatagramSocket(port);
        statusLog = new ArrayList<>();
        receiveBuffer = new byte[1024];
    }

    private void handleUDPRequests() {
        try {

            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            serverSocket.receive(receivePacket);
            String receiveString = new String(receivePacket.getData());
            statusLog.add(parseStatus(receiveString));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SensorStatus parseStatus(String stat) {
        String[] data = stat.split(";");

        return new SensorStatus(data[0], Float.parseFloat(data[1]));

    }

    ArrayList<SensorStatus> getStatusLog() {
        return statusLog;
    }

    @Override
    public void run() {
        System.out.println("UDP Server started!");
        while (true) {
            handleUDPRequests();
        }
    }
}
